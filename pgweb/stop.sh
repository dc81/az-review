#!/bin/sh
echo -n "Stopping pgweb ..."
kill -9 `ps ax | grep pgweb | grep bind | awk '{print $1}'` 2>/dev/null
if [ -z `ps ax | grep pgweb | grep bind | awk '{print $1}'` ] ; 
then   
    echo " Completed"
else   
    echo " Failed !"
fi