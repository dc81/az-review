#!/bin/sh
if [ -x "/usr/local/bin/pgweb" ] ;
then
    echo "Running pgweb, please press ENTER twice"
    /usr/local/bin/pgweb --bind=0.0.0.0 &
else
    echo "No pgweb installation found"
fi
