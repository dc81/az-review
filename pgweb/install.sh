#!/bin/sh
echo "Installing pgweb ..."
curl -s https://api.github.com/repos/sosedoff/pgweb/releases/latest \
  | grep linux_amd64.zip \
  | grep download \
  | cut -d '"' -f 4 \
  | wget -qi - \
  && unzip -o pgweb_linux_amd64.zip \
  && rm pgweb_linux_amd64.zip \
  && sudo mv pgweb_linux_amd64 /usr/local/bin/pgweb
if [ -x "/usr/local/bin/pgweb" ] ;
then
    echo "pgweb is installed successfully"
else
    echo "No pgweb executable"
fi